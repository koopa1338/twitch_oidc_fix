FROM rust as builder
WORKDIR /app
COPY . .
RUN cargo build --release



FROM debian
COPY --from=builder /app/target/release/twitch_oidc_fix /twitch_oidc_fix
CMD ["/twitch_oidc_fix"]
