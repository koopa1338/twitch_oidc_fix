# Twitch OIDC Fix

![Authentik Single Sign On page](images/authentik.png)

tldr; Use Twitch for SSO (Single Sign On) or Generic OIDC Client, just a small bridge/middleware. :)

The Twitch-API can be a harsh place, the developers are not following the OAuth 2.0 Authorization Framework ([RFC6749](https://datatracker.ietf.org/doc/html/rfc6749) standard.)
I'm using a tool like [Keycloak](https://www.keycloak.org/) and [Authentik](https://goauthentik.io/), so it was impossible to use the `Twitch-API` for `SSO`.
One reason is the `/token` endpoint, the RFC says that the `Access Token` should contain [scopes](And that's the reason why Tools like ) (which is a space separated list), but Twitch "is funny enough" to NOT follow the [RFC6749](https://datatracker.ietf.org/doc/html/rfc6749) and things "we could implement it differently", so the response is an array of strings... T.T \
For fuck's sake ... Imagine there is a standard and your company is fighting against it... T.T

## Fixing their API

### /authorization

This route is just redirect you to `https://id.twitch.tv/oauth2/authorize`, but
it's removing all scopes which are not supported. \
[Authentik](https://goauthentik.io/) for example adds the `profile` and `email` scope.
Which can't be removed and is not working with the Twitch API.
So this middleware is getting rid of it and redirects you.

### /token

The `/token` endpoint contains an `access_token` which contains all the `scopes`.
But Twitch is not following the [RFC6749](https://datatracker.ietf.org/doc/html/rfc6749).
So the middleware is convert the `string array` from Twitch to the `space sperarated list`.

### /userinfo

Same problem like in the `/token` endpoint, the scopes are causing problems.

## Installation

You can set the following environments:

- `RUST_LOG` (default: warn)
- `HOST` (default: 0.0.0.0)
- `PORT` (port: 8080)
- `DEFAULT_CLAIMS_ID_TOKEN` (default: `picture,preferred_username,updated_at`)
- `DEFAULT_CLAIMS_USERINFO` (default: `picture,preferred_username,updated_at`)

The `claims` can be set in the url query, but you can also set a default value
(which will be used, if the query is not used).
See [Requesting Claims](https://dev.twitch.tv/docs/authentication/getting-tokens-oidc/#requesting-claims)
to understand what are the `claims` for.

### Docker

```bash
docker run \
    -p 8080:8080 \
    -e RUST_LOG=info \
    -e DEFAULT_CLAIMS_ID_TOKEN="email,email_verified,picture,preferred_username,updated_at" \
    -e DEFAULT_CLAIMS_USERINFO="email,email_verified,picture,preferred_username,updated_at" \
    registry.gitlab.com/kerkmann/twitch_oidc_fix
```

### Docker-compose

```yaml
twitch_oidc_fix:
  image: registry.gitlab.com/kerkmann/twitch_oidc_fix
  restart: unless-stopped
  ports:
    - 8080:8080
  environment:
    - RUST_LOG=info
    - DEFAULT_CLAIMS_ID_TOKEN="email,email_verified,picture,preferred_username,updated_at"
    - DEFAULT_CLAIMS_USERINFO="email,email_verified,picture,preferred_username,updated_at"
```

### Build it yourself (binary)

You can build the binary instead:

```bash
git clone https://gitlab.com/kerkmann/twitch_oidc_fix
cargo build --release
```

or build it from [crates.io](https://crates.io):

```bash
cargo install twitch_oidc_fix
```

Or you just download the binary [here](https://gitlab.com/kerkmann/twitch_oidc_fix/-/jobs/artifacts/main/raw/twitch_oidc_fix?job=release)

## Trademarks

I am not affiliated, associated, authorized, endorsed by, or in any way officially connected with Twitch Interactive, Inc. in USA and/or other countries.
